package com.kawaiilapen.hw4.controller

import jakarta.xml.bind.DatatypeConverter
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.util.MultiValueMap
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.view.RedirectView
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardCopyOption
import java.security.MessageDigest

@Controller
class DraftController {
    val data = mutableMapOf<String, Any>()
    var fData: Path? = null

    @PostMapping("/save")
    @CrossOrigin(origins = ["http://127.0.0.1:5500"])
    fun saveDraft(
        @RequestParam formFields: MultiValueMap<String, String>,
        @RequestParam("file", required = false) file: MultipartFile?
    ): ResponseEntity<String> {
        if (formFields["numIn"]?.get(0) == "4") {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Amaterasu is displeased with you")
        }
        data.clear()

        formFields.forEach { (key, value) ->
            data[key] = value.joinToString()
        }

        if (file != null && !file.isEmpty) {
            file.let {
                fData = Files.createTempFile("tmp-", ".tmp")
                it.inputStream.use { inputStream ->
                    Files.copy(inputStream, fData!!, StandardCopyOption.REPLACE_EXISTING)
                }
            }
            val fileSize = file.size
            val fileHash = file.inputStream.use { inputStream ->
                val buffer = inputStream.readBytes()
                val md = MessageDigest.getInstance("MD5")
                val digest = md.digest(buffer)
                DatatypeConverter.printHexBinary(digest).toUpperCase()
            }

            data["fileSize"] = fileSize
            data["fileHash"] = fileHash
        }

        return ResponseEntity.ok("Draft saved")
    }

    @PostMapping("/publish")
    @CrossOrigin(origins = ["http://127.0.0.1:5500"])
    fun publishDraft(
        @RequestParam formFields: MultiValueMap<String, String>,
        @RequestParam("file", required = false) file: MultipartFile?
    ): RedirectView {
        data.clear()

        formFields.forEach { (key, value) ->
            data[key] = value.joinToString()
        }

        if (file != null && !file.isEmpty) {
            val fileSize = file.size
            val fileHash = file.inputStream.use { inputStream ->
                val buffer = inputStream.readBytes()
                val md = MessageDigest.getInstance("MD5")
                val digest = md.digest(buffer)
                DatatypeConverter.printHexBinary(digest).toUpperCase()
            }

            data["fileSize"] = fileSize
            data["fileHash"] = fileHash
        }

        return RedirectView("/sdata")
    }

    @GetMapping("/sdata")
    @CrossOrigin(origins = ["http://127.0.0.1:5500"])
    fun result(model: Model): String {
        model.addAttribute("data", data)
        return "sdata"
    }

    @GetMapping("/restore")
    @CrossOrigin(origins = ["http://127.0.0.1:5500"])
    fun loadDraft(): ResponseEntity<Map<String, Any>> {
        return ResponseEntity.ok(data)
    }

    @GetMapping("/restore-file", produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE])
    @CrossOrigin(origins = ["http://127.0.0.1:5500"])
    fun loadDraftFile(): ResponseEntity<ByteArray> {
        return fData?.let {
            ResponseEntity.ok(Files.readAllBytes(it))
        } ?: ResponseEntity.notFound().build()
    }
}
