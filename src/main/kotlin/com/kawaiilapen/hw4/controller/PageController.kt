package com.kawaiilapen.hw4.controller

import jakarta.xml.bind.DatatypeConverter
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.util.MultiValueMap
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import java.security.MessageDigest


@Controller
class GreetingController {
    @RequestMapping("/")
    fun form(model: Model): String {
        return "index"
    }

    @PostMapping("/submit")
    fun submit(
        @RequestParam formFields: MultiValueMap<String, String>,
        @RequestParam("fileField") fileField: MultipartFile,
        redirectAttributes: RedirectAttributes
    ): String {
        val dataForDisplay = hashMapOf<String, Any>()
        formFields.forEach { (key, value) ->
            dataForDisplay[key] = value.joinToString()
        }

        if (!fileField.isEmpty) {
            val fileSize = fileField.size
            val fileHash = fileField.inputStream.use { inputStream ->
                val buffer = inputStream.readBytes()
                val md = MessageDigest.getInstance("MD5")
                val digest = md.digest(buffer)
                DatatypeConverter.printHexBinary(digest).toUpperCase()
            }

            dataForDisplay["fileSize"] = fileSize
            dataForDisplay["fileHash"] = fileHash
        }

        redirectAttributes.addFlashAttribute("submittedData", dataForDisplay)

        return "redirect:/result"
    }

    @RequestMapping("/result")
    fun result(model: Model): String {
        return "result"
    }
}